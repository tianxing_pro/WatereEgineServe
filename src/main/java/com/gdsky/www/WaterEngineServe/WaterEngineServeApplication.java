package com.gdsky.www.WaterEngineServe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WaterEngineServeApplication {

	public static void main(String[] args) {
		SpringApplication.run(WaterEngineServeApplication.class, args);
	}


}
