package com.gdsky.www.WaterEngineServe.controller;
import com.gdsky.www.WaterEngineServe.pojo.BaseWaterStation;
import com.gdsky.www.WaterEngineServe.pojo.Response;
import com.gdsky.www.WaterEngineServe.service.BaseWaterStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/station")
public class BaseWaterStationController {

    @Autowired
    private BaseWaterStationService baseWaterStationService;

    @GetMapping("/base-stations")
    public List<BaseWaterStation> getStations(
            @RequestParam(required = false) String value
    ) {
        return baseWaterStationService.findStationsByValue(value);
    }

    @PostMapping("/saveStation")
    public Response saveStation(@RequestBody BaseWaterStation station) {
      int code =  baseWaterStationService.updateStation(station);
        Response res = new Response();
        if (code > 0) {
            return res.responseSuccess(1);
        }
        return res.responseFail(0);
    }
}
