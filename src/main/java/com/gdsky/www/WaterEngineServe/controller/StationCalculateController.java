package com.gdsky.www.WaterEngineServe.controller;
import com.gdsky.www.WaterEngineServe.pojo.*;
import com.gdsky.www.WaterEngineServe.service.StationCalculateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/station")
public class StationCalculateController {

    @Autowired
    private StationCalculateService stationCalculateService;

    @GetMapping("/calculate")
    public List<StationCalculate> findByStationCode(@RequestParam(required = false) String stationCode) {
        return stationCalculateService.findByStationCode(stationCode);
    }

    // 查询泄流管
    @GetMapping("/xieliu/guan")
    public List<XieLiuGuan> findXieLiuGuanByStationCode(@RequestParam(required = false) String stationCode) {
        return stationCalculateService.findXieLiuGuanByStationCode(stationCode);
    }

    // 更新泄流管
    @PostMapping("/update/xieliuguan")
    public Response updateXieLiuGuan(@RequestBody XieLiuGuan xieLiuGuan) {
        int code = stationCalculateService.updateXieLiuGuan(xieLiuGuan);
        Response res = new Response();
        if (code > 0) {
            return res.responseSuccess(1);
        }
        return res.responseFail(0);
    }

    // 查询泄流闸
    @GetMapping("/xieliu/zha")
    public List<XieLiuZha> findXieLiuZhaByStationCode(@RequestParam(required = false) String stationCode) {
        return stationCalculateService.findXieLiuZhaByStationCode(stationCode);
    }

    // 更新泄流闸
    @PostMapping("/update/xieliuzha")
    public Response updateXieLiuZha(@RequestBody XieLiuZha xieLiuZha) {
        int code = stationCalculateService.updateXieLiuZha(xieLiuZha);
        Response res = new Response();
        if (code > 0) {
           return res.responseSuccess(1);
        }
        return res.responseFail(0);
    }

    // 查询生态机组
    @GetMapping("/shengtai/plants")
    public List<ShengTaiPlants> findShengTaiPlantsByStationCode(@RequestParam(required = false) String stationCode) {
        return stationCalculateService.findShengTaiPlantsByStationCode(stationCode);
    }

    // 更新生态组数据
    @PostMapping("/delete-and-insert")
    public void deleteAndInsertPlants(@RequestBody List<ShengTaiPlants> plants) {
        stationCalculateService.deleteAndInsertPlants(plants);
    }

}
