package com.gdsky.www.WaterEngineServe.controller;
import com.aliyun.oss.OSSClient;
import com.gdsky.www.WaterEngineServe.pojo.FeedStationsByCode;
import com.gdsky.www.WaterEngineServe.pojo.Response;
import com.gdsky.www.WaterEngineServe.pojo.StationImage;
import com.gdsky.www.WaterEngineServe.pojo.UploadImagesRequest;
import com.gdsky.www.WaterEngineServe.service.OssClient;
import com.gdsky.www.WaterEngineServe.service.StationImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/station")
public class StationImagesController {

    @Autowired
    private StationImageService service;

    // 上传文件
    @PostMapping("/uploadImage")
    public Map<String, Object> uploadImage(@RequestParam("file") MultipartFile file) {
        OssClient ossClient = new OssClient();
        Map<String, Object> fileInfo = null;
        try {
            // 上传文件到 OSS
            String objectName = file.getOriginalFilename();
            System.out.println(objectName);
            fileInfo = ossClient.uploadFile(objectName, file.getInputStream());
        } catch (IOException e) {
            System.out.println(e);
        } finally {
            ossClient.shutdown();
        }
        return fileInfo;
    }

    // 将上传文件信息、绑定文件信息 同步到数据库
    @PostMapping("/insert-station-images")
    public Response uploadImages(@RequestBody List<StationImage> images) {
        // 异步插入图片记录到数据库
        int code = service.insertImage(images);
        Response res = new Response();
        if (code > 0) {
            return res.responseSuccess(1);
        }
        return res.responseFail(0);
    }

    @GetMapping("/queryImagesByStationCode")
    public List<Map<String, Object>> queryImagesByStationCode(@RequestParam(required = true) String stationCode) {
        List<StationImage> images = service.findByStationCode(stationCode);
        Map<String, List<StationImage>> groupedImages = service.groupByUploadTime(images);
        // 转换 Map 为嵌套数组
        List<Map<String, Object>> result = groupedImages.entrySet().stream()
                .map(entry -> {
                    Map<String, Object> group = new HashMap<>();
                    group.put("uploadTime", entry.getKey());
                    group.put("message", entry.getValue().get(0).getMessage());
                    group.put("images", entry.getValue());
                    return group;
                }).sorted((m1, m2) -> {
                    String time1 = (String) m1.get("uploadTime");
                    String time2 = (String) m2.get("uploadTime");
                    return time2.compareTo(time1); // 降序排序
                })
                .collect(Collectors.toList());
        return result;
    }

    @GetMapping("/queryImagesByStationCodes")
    public List<FeedStationsByCode> queryImagesByStationCodes(@RequestParam(required = true) List<String> stationCodes) {

        List<FeedStationsByCode> results = new ArrayList<>();

        for (String stationCode : stationCodes) {
            List<Map<String, Object>> result = queryImagesByStationCode(stationCode);
            // 创建 FeedStationsByCode 对象
            FeedStationsByCode feedStation = new FeedStationsByCode();
            feedStation.setStationCode(stationCode);

//            {
//                "stationCode": "1001",
//                "images": [
//                    {
//                        "uploadTime": "2021-08-01 12:00:00",
//                        "message": "这是一张图片",
//                        "images": [
//                            {
//                                "stationCode": "1001",
//                                "message": "这是一张图片",
//                                "imageId": "1",
//                                "uploadTime": "2021-08-01 12:00:00",
//                                "imageSize": 1024,
//                                "extension": "jpg",
//                                "fileName": "1.jpg",
//                                "imageUrl": "https://www.gdsky.com/1.jpg"
//                            }
//                        ]
//                    }
//                ]
//            }
//            feedStation.setFeedImageUrls(results.map(_result -> _result.get("images")));

            results.add(feedStation);
        }
        return result;

    }

    @PostMapping("/batchOfImageIds")
    public List<StationImage> getImagesByImageIds(@RequestBody List<String> imageIds) {
        List<StationImage> images = service.getImagesByImageIds(imageIds);
        return images;
    }


    // 根据 station_code 删除图片记录
    @PostMapping("/deleteImages")
    public ResponseEntity<Void> deleteImagesByStationCode(@RequestParam String stationCode) {
        int deletedCount = service.deleteImagesByStationCode(stationCode);
        if (deletedCount > 0) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // 更新图片信息
    @PostMapping("/updateImages")
    public Response updateImages(@RequestBody UploadImagesRequest request) {
        service.updateImages(request);
        Response res = new Response();
        return res.responseSuccess(1);
    }

    @DeleteMapping("/deleteImagesByStationCodeAndUploadTime")
    public Response deleteImagesByStationCodeAndUploadTime(
            @RequestParam String stationCode,
            @RequestParam String uploadTime) {
        int deletedCount = service.deleteImagesByStationCodeAndUploadTime(stationCode, uploadTime);
        Response res = new Response();
        if (deletedCount > 0) {
            return res.responseSuccess(1);
        }
        return res.responseFail(0);
    }
}
