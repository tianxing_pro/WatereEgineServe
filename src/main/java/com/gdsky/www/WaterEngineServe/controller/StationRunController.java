package com.gdsky.www.WaterEngineServe.controller;
import com.gdsky.www.WaterEngineServe.pojo.StationRun;
import com.gdsky.www.WaterEngineServe.service.StationRunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/station")
public class StationRunController {

    @Autowired
    private StationRunService stationRunService;

    @GetMapping("/run")
    public List<StationRun> getStationRunByCode(@RequestParam(required = true) String stationCode) {
        return stationRunService.findByStationCode(stationCode);
    }
}

