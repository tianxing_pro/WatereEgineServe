package com.gdsky.www.WaterEngineServe.mapper;
import com.gdsky.www.WaterEngineServe.pojo.BaseWaterStation;
import com.gdsky.www.WaterEngineServe.pojo.Response;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;

@Mapper  // 在运行时，会自动生成该接口的实现类对象(代理对象) 并且将该对象交给 IOC 容器管理
public interface StationBaseMapper {

    @Select({
            "<script>",
            "SELECT * FROM base_water_station",
            "WHERE 1=1",
            "<if test='value != null and value != \"\"'>",
            "AND (station_code LIKE CONCAT('%', #{value}, '%')",
            "OR station_name LIKE CONCAT('%', #{value}, '%')",
            "OR towns_name LIKE CONCAT('%', #{value}, '%')",
            "OR manager LIKE CONCAT('%', #{value}, '%'))",
            "</if>",
            "</script>"
    })
    List<BaseWaterStation> findStationsByValue(@Param("value") String value);

    @Select("select * from base_water_station")
    List<BaseWaterStation> list();

    @UpdateProvider(type = SqlBuilder.class, method = "buildUpdateStation")
    int updateStation(@Param("station") BaseWaterStation station);

    class SqlBuilder {
    public String buildUpdateStation(@Param("station") BaseWaterStation station) {
        return new SQL() {{
            UPDATE("base_water_station");
            if (station.getStationName() != null) SET("station_name=#{stationName}");
            if (station.getCityName() != null) SET("city_name = #{station.cityName}");
            if (station.getCountyName() != null) SET("county_name = #{station.countyName}");
            if (station.getTownsName() != null) SET("towns_name = #{station.townsName}");
            if (station.getLocation() != null) SET("location = #{station.location}");
            if (station.getManager() != null) SET("manager = #{station.manager}");
            if (station.getDevelopType() != null) SET("develop_type = #{station.developType}");
            if (station.getOwnerType() != null) SET("owner_type = #{station.ownerType}");
            if (station.getRunStatus() != null) SET("run_status = #{station.runStatus}");
            if (station.getContactPerson() != null) SET("contact_person = #{station.contactPerson}");
            if (station.getPhone() != null) SET("phone = #{station.phone}");
            if (station.getClassifyType() != null) SET("classify_type = #{station.classifyType}");
            if (station.getSafetyAppraisalType() != null) SET("safety_appraisal_type = #{station.safetyAppraisalType}");
            if (station.getIntegratedUtilization() != null) SET("integrated_utilization = #{station.integratedUtilization}");
            if (station.getRiverName() != null) SET("river_name = #{station.riverName}");
            if (station.getDamLongitude() != null) SET("dam_longitude = #{station.damLongitude}");
            if (station.getDamLatitude() != null) SET("dam_latitude = #{station.damLatitude}");
            if (station.getPlantLongitude() != null) SET("plant_longitude = #{station.plantLongitude}");
            if (station.getPlantLatitude() != null) SET("plant_latitude = #{station.plantLatitude}");
            if (station.getReservoirName() != null) SET("reservoir_name = #{station.reservoirName}");

            WHERE("station_code = #{station.stationCode}");
        }}.toString();
    }
    }

}
