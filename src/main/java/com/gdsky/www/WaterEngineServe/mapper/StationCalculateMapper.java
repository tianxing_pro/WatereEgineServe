package com.gdsky.www.WaterEngineServe.mapper;
import com.gdsky.www.WaterEngineServe.pojo.ShengTaiPlants;
import com.gdsky.www.WaterEngineServe.pojo.StationCalculate;
import com.gdsky.www.WaterEngineServe.pojo.XieLiuGuan;
import com.gdsky.www.WaterEngineServe.pojo.XieLiuZha;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.jdbc.SQL;
import java.util.List;

@Mapper
public interface StationCalculateMapper {

    @Select("SELECT * FROM station_calculate WHERE station_code = #{stationCode}")
    List<StationCalculate> findByStationCode(@Param("stationCode") String stationCode);

    // 查询泄流管
    @Select("SELECT * FROM station_xieliu_guan WHERE station_code = #{stationCode}")
    List<XieLiuGuan> findXieLiuGuanByStationCode(@Param("stationCode") String stationCode);

    // 查询泄流闸
    @Select("SELECT * FROM station_xieliu_zha WHERE station_code = #{stationCode}")
    List<XieLiuZha> findXieLiuZhaByStationCode(@Param("stationCode") String stationCode);

    // 查询生态机组
    @Select("SELECT * FROM station_shengtai_plants WHERE station_code = #{stationCode}")
    List<ShengTaiPlants> findShengTaiPlantsByStationCode(@Param("stationCode") String stationCode);

    // 更新 泄流管
    @UpdateProvider(type = SqlBuilder.class, method = "buildUpdateXieLiuGuan")
    int updateXieLiuGuanByStationCode(@Param("xieLiuGuan") XieLiuGuan xieLiuGuan);
    // 更新 泄流闸
    @UpdateProvider(type = SqlBuilder.class, method = "buildUpdateXieLiuZha")
    int updateXieLiuZhaByStationCode(@Param("xieLiuZha") XieLiuZha xieLiuZha);

    class SqlBuilder {
        public String buildUpdateXieLiuGuan(@Param("xieLiuGuan") XieLiuGuan xieLiuGuan) {
            return new SQL(){{
                UPDATE("station_xieliu_guan");
                if (xieLiuGuan.getQ() != null) {
                    SET("Q = #{xieLiuGuan.Q}");
                }
                if (xieLiuGuan.getV() != null) {
                    SET("V = #{xieLiuGuan.V}");
                }
                if (xieLiuGuan.getH() != null) {
                    SET("H = #{xieLiuGuan.H}");
                }
                if (xieLiuGuan.getL() != null) {
                    SET("L = #{xieLiuGuan.L}");
                }
                if (xieLiuGuan.getD() != null) {
                    SET("D = #{xieLiuGuan.D}");
                }
                if (xieLiuGuan.getN() != null) {
                    SET("n = #{xieLiuGuan.n}");
                }
                if (xieLiuGuan.getR() != null) {
                    SET("R = #{xieLiuGuan.R}");
                }
                if (xieLiuGuan.getA() != null) {
                    SET("A = #{xieLiuGuan.A}");
                }
                if (xieLiuGuan.getE() != null) {
                    SET("E = #{xieLiuGuan.E}");
                }
                if (xieLiuGuan.getX() != null) {
                    SET("x = #{xieLiuGuan.x}");
                }
                WHERE("station_code = #{xieLiuGuan.stationCode}");
            }}.toString();
        }

        public String buildUpdateXieLiuZha(@Param("xieLiuZha") XieLiuZha xieLiuZha) {
            return new SQL(){{
                UPDATE("station_xieliu_zha");
                if (xieLiuZha.getQ() != null) {
                    SET("Q = #{xieLiuZha.Q}");
                }
                if (xieLiuZha.getE() != null) {
                    SET("e = #{xieLiuZha.e}");
                }
                if (xieLiuZha.getB() != null) {
                    SET("B = #{xieLiuZha.B}");
                }
                if (xieLiuZha.getH() != null) {
                    SET("H = #{xieLiuZha.H}");
                }
                if (xieLiuZha.getΜ() != null) {
                    SET("μ = #{xieLiuZha.μ}");
                }
                if (xieLiuZha.getΘ() != null) {
                    SET("θ = #{xieLiuZha.θ}");
                }
                WHERE("station_code = #{xieLiuZha.stationCode}");
            }}.toString();
        }
    }

    // 更新 泄流闸
    @Delete("<script>" +
            "DELETE FROM station_shengtai_plants WHERE station_code IN " +
            "<foreach item='code' collection='stationCodes' open='(' separator=',' close=')'>" +
            "#{code}" +
            "</foreach>" +
            "</script>")
    void deleteByStationCodes(@Param("stationCodes") List<String> stationCodes);

    @Insert("<script>" +
            "INSERT INTO station_shengtai_plants (station_code, eco_flow_type) VALUES " +
            "<foreach item='item' collection='plants' separator=','>" +
            "(#{item.stationCode}, #{item.ecoFlowType})" +
            "</foreach>" +
            "</script>")
    void insertPlants(@Param("plants") List<ShengTaiPlants> plants);

}

