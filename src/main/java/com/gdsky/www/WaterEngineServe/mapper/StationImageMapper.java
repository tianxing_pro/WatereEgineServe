package com.gdsky.www.WaterEngineServe.mapper;
import com.gdsky.www.WaterEngineServe.pojo.StationImage;
import java.util.List;
import org.apache.ibatis.annotations.*;

@Mapper
public interface StationImageMapper {

    @Insert("<script>" +
            "INSERT INTO station_images (station_code, message, image_id, upload_time, image_size, extension, file_name, image_url) VALUES " +
            "<foreach item='item' collection='images' separator=','>" +
            "(#{item.stationCode}, #{item.message}, #{item.imageId}, #{item.uploadTime}, #{item.imageSize}, #{item.extension}, #{item.fileName}, #{item.imageUrl})" +
            "</foreach>" +
            "</script>")
    int insertImages(@Param("images") List<StationImage> images);

    @Select("SELECT * FROM station_images WHERE station_code = #{stationCode} ORDER BY upload_time ASC")
    List<StationImage> findByStationCode(@Param("stationCode") String stationCode);

    @Select("<script>" +
            "SELECT * FROM station_images WHERE image_id IN " +
            "<foreach item='item' index='index' collection='imageIds' open='(' separator=',' close=')'>" +
            "#{item}" +
            "</foreach>" +
            "</script>")
    List<StationImage> findByImageIds(@Param("imageIds") List<String> imageIds);

    // 根据 station_code 删除图片记录
    @Delete("DELETE FROM station_images WHERE station_code = #{stationCode}")
    int deleteByStationCode(@Param("stationCode") String stationCode);

    // 根据 station_code 和 upload_time 删除图片记录
    @Delete("DELETE FROM station_images WHERE station_code = #{stationCode} AND upload_time = #{uploadTime}")
    int deleteByStationCodeAndUploadTime(@Param("stationCode") String stationCode, @Param("uploadTime") String uploadTime);
}
