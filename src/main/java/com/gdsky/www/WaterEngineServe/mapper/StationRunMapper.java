package com.gdsky.www.WaterEngineServe.mapper;
import com.gdsky.www.WaterEngineServe.pojo.StationRun;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface StationRunMapper {

    @Select("SELECT * FROM station_run WHERE station_code = #{stationCode}")
    List<StationRun> findByStationCode(@Param("stationCode") String stationCode);
}