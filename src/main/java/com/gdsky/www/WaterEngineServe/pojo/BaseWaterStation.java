package com.gdsky.www.WaterEngineServe.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class BaseWaterStation {
    private String stationCode;
    private String stationName;
    private String cityName;
    private String countyName;
    private String townsName;
    private String location;
    private String manager;
    private String developType;
    private String ownerType;
    private String runStatus;
    private String contactPerson;
    private String phone;
    private String classifyType;
    private String safetyAppraisalType;
    private String integratedUtilization;
    private String riverName;
    private BigDecimal damLongitude;
    private BigDecimal damLatitude;
    private BigDecimal plantLongitude;
    private BigDecimal plantLatitude;
    private String reservoirName;

}

