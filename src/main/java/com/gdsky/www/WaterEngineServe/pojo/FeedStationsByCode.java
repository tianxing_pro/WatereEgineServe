package com.gdsky.www.WaterEngineServe.pojo;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor

public class FeedStationsByCode {

    private List<StationImage> feedImages;
    private String stationCode;
    private String stationName;

    public FeedStationsByCode() {

    }
}
