package com.gdsky.www.WaterEngineServe.pojo;

import lombok.Data;

@Data
public class Response {
    private int code;
    private String msg;
    private Object data;

    public Response(){}
    public Response(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public Response responseSuccess(Object data) {
        this.code = 200;
        this.msg =  "更新成功";
        this.data = data;
        return this;
    }
    public Response responseFail(Object data) {
        this.code = 400;
        this.msg =  "更新失败";
        this.data = data;
        return this;
    }
}
