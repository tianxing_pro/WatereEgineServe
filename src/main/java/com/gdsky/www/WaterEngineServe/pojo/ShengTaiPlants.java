package com.gdsky.www.WaterEngineServe.pojo;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ShengTaiPlants {

    private String stationCode;
    private String ecoFlowType;
    private String Q;
    private String N;
    private String η = "0.8";
    private String h;

}
