package com.gdsky.www.WaterEngineServe.pojo;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StationCalculate {

    private String stationCode; // 电站代码
    private Float waterHead; // 电站设计水头
    private String waterHeadUnit = "m"; // 水头单位 m
    private Float riverWayLong; // 厂坝间河道长度（km）
    private String riverWayLongUnit = "km"; // 厂坝间河道长度单位（km）
    private Float riverWayDehydrationLong; // 厂坝间脱水长度（km）
    private String riverWayDehydrationLongUnit = "km"; // 厂坝间脱水长度 - 单位
    private Float ecoFlow; // 生态流量核定值
    private String ecoFlowType; // 生态泄流设施
    private Float damHeight; // 坝高
    private String damHeightUnit = "m"; // 坝高单位
}
