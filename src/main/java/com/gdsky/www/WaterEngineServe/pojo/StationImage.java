package com.gdsky.www.WaterEngineServe.pojo;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StationImage {

    private Long id;
    private String stationCode;
    private String message;
    private String imageId;
    private String uploadTime;
    private Long imageSize;
    private String extension;
    private String fileName;
    private String imageUrl;

}
