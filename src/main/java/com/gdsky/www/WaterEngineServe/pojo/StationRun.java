package com.gdsky.www.WaterEngineServe.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StationRun {

    private String stationCode; // 电站代码
    private Float totalFillingContent; // 装机容量
    private String totalFillingContentUnit = "kW"; // 装机容量单位
    private Float totalReservoirStock; // 水库（山塘）总库容
    private String totalReservoirStockUnit = "万m³"; // 水库（山塘）总库容（万m³）
    private Float catchmentArea; // 集水面积
    private String catchmentAreaUnit = "k㎡"; // 集水面积单位
    private Float averageAnnualDischarge; // 多年平均流量
    private String averageAnnualDischargeUnit = "m³/s"; // 多年平均流量（m³/s）
    private String adjustPerformance; // 水库调节性
    private String plantNetwork; // 厂房通讯网络条件
    private String damNetwork; // 大坝通讯网络条件
    private String ecologicalRelease; // 是否有生态泄放要求
}
