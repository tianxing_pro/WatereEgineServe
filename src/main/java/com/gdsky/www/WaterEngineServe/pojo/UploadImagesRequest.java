package com.gdsky.www.WaterEngineServe.pojo;
import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.List;

@Data
@AllArgsConstructor
public class UploadImagesRequest {
    List<StationImage> images;
    String uploadTime;
}
