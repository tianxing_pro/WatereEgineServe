package com.gdsky.www.WaterEngineServe.pojo;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class XieLiuGuan {

    private String stationCode;

    private String ecoFlowType;

    private String Q;

    private String V;

    private String H;

    private String L;

    private String D;

    private String n = "0.013";

    private String R;

    private String A;

    private String E; // ξ

    private String x;

}
