package com.gdsky.www.WaterEngineServe.pojo;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class XieLiuZha {

    private String stationCode;
    private String ecoFlowType;
    private String Q;
    private String e;
    private String B;
    private String H;
    private String μ;
    private String θ;

}
