package com.gdsky.www.WaterEngineServe.service;
import com.gdsky.www.WaterEngineServe.mapper.StationBaseMapper;
import com.gdsky.www.WaterEngineServe.pojo.BaseWaterStation;
import com.gdsky.www.WaterEngineServe.pojo.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class BaseWaterStationService {

    @Autowired
    private StationBaseMapper baseWaterStationMapper;

    public List<BaseWaterStation> findStationsByValue(String value) {
        return baseWaterStationMapper.findStationsByValue(value);
    }

    // 更新基础数据
    public int updateStation(BaseWaterStation station) {
       return baseWaterStationMapper.updateStation(station);
    }



}
