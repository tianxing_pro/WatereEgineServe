package com.gdsky.www.WaterEngineServe.service;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class OssClient {

    private static final String ENDPOINT = "https://oss-cn-beijing.aliyuncs.com"; // 替换为你的 OSS 端点
    private static final String ACCESS_KEY_ID = "LTAI5tPY26MjzBJVrg8JNsDm"; // 替换为你的 AccessKey ID
    private static final String ACCESS_KEY_SECRET = "A3GbmZCnRrGIq12IyZq3mmrLWJ4BoJ"; // 替换为你的 AccessKey Secret
    private static final String BUCKET_NAME = "water-engine-bucket"; // 替换为你的存储空间名称

    private OSS ossClient;

    public OssClient() {
        this.ossClient = new OSSClientBuilder().build(ENDPOINT, ACCESS_KEY_ID, ACCESS_KEY_SECRET);
    }

    public Map<String, Object> uploadFile(String objectName, InputStream inputStream) throws IOException {
        // 获取文件大小
        long fileSize = inputStream.available();
        PutObjectRequest putObjectRequest = new PutObjectRequest(BUCKET_NAME, objectName, inputStream);
        PutObjectResult result = ossClient.putObject(putObjectRequest);

        // 构建返回数据
        Map<String, Object> data = new HashMap<>();
        data.put("createdAt", System.currentTimeMillis());
        data.put("extension", getFileExtension(objectName));
        data.put("filename", objectName);
        data.put("id", result.getRequestId().hashCode()); // 简单生成一个文件 ID
        data.put("size", fileSize);
        data.put("uri", "https://" + BUCKET_NAME + ".oss-cn-beijing.aliyuncs.com/" + objectName);
//        data.put("uri", ENDPOINT.replace("http://", "https://") + "/" + BUCKET_NAME + "/" + objectName);
        return data;
    }

    private String getFileExtension(String filename) {
        if (filename.contains(".")) {
            return filename.substring(filename.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    public void shutdown() {
        if (ossClient != null) {
            ossClient.shutdown();
        }
    }
}
