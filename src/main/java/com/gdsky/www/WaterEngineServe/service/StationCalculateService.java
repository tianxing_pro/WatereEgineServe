package com.gdsky.www.WaterEngineServe.service;
import com.gdsky.www.WaterEngineServe.mapper.StationCalculateMapper;
import com.gdsky.www.WaterEngineServe.pojo.ShengTaiPlants;
import com.gdsky.www.WaterEngineServe.pojo.StationCalculate;
import com.gdsky.www.WaterEngineServe.pojo.XieLiuGuan;
import com.gdsky.www.WaterEngineServe.pojo.XieLiuZha;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StationCalculateService {

    @Autowired
    private StationCalculateMapper stationCalculateMapper;

    public List<StationCalculate> findByStationCode(String stationCode) {
        return stationCalculateMapper.findByStationCode(stationCode);
    }
    // 查询泄流管
    public List<XieLiuGuan> findXieLiuGuanByStationCode(@Param("stationCode") String stationCode) {
        return stationCalculateMapper.findXieLiuGuanByStationCode(stationCode);
    }

    // 更新泄流管
    public int updateXieLiuGuan(XieLiuGuan xieLiuGuan) {
        return stationCalculateMapper.updateXieLiuGuanByStationCode(xieLiuGuan);
    }

    // 查询泄流闸
    public List<XieLiuZha> findXieLiuZhaByStationCode(@Param("stationCode") String stationCode) {
        return stationCalculateMapper.findXieLiuZhaByStationCode(stationCode);
    }

    // 更新泄流闸
    public int updateXieLiuZha(XieLiuZha xieLiuZha) {
        return stationCalculateMapper.updateXieLiuZhaByStationCode(xieLiuZha);
    }

    // 查询生态机组
    public List<ShengTaiPlants> findShengTaiPlantsByStationCode(@Param("stationCode") String stationCode) {
        return stationCalculateMapper.findShengTaiPlantsByStationCode(stationCode);
    }

    // 更新生态机组
    @Transactional
    public void deleteAndInsertPlants(List<ShengTaiPlants> plants) {
        // 提取 stationCode 列表
        List<String> stationCodes = plants.stream()
                .map(ShengTaiPlants::getStationCode)
                .distinct()
                .collect(Collectors.toList());
        // 删除旧数据
        stationCalculateMapper.deleteByStationCodes(stationCodes);
        // 插入新数据
        stationCalculateMapper.insertPlants(plants);
    }

}
