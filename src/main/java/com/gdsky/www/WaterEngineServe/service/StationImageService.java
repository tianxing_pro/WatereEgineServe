package com.gdsky.www.WaterEngineServe.service;
import com.gdsky.www.WaterEngineServe.mapper.StationImageMapper;
import com.gdsky.www.WaterEngineServe.pojo.StationImage;
import com.gdsky.www.WaterEngineServe.pojo.UploadImagesRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class StationImageService {

    @Autowired
    private StationImageMapper mapper;

    // 将数据写入数据库
    @Transactional
    public int insertImage(List<StationImage> images) {
       return mapper.insertImages(images);
    }

    public List<StationImage> findByStationCode(String stationCode) {
        return mapper.findByStationCode(stationCode);
    }

    public Map<String, List<StationImage>> groupByUploadTime(List<StationImage> images) {
        return images.stream()
                .collect(Collectors.groupingBy(image -> image.getUploadTime().toString()));
    }
    // 根据imageids 获取图片信息
    public List<StationImage> getImagesByImageIds(List<String> imageIds) {
        return mapper.findByImageIds(imageIds);
    }

    // 根据 station_code 删除图片记录
    public int deleteImagesByStationCode(String stationCode) {
        return mapper.deleteByStationCode(stationCode);
    }

    // 更新图片信息
    @Transactional
    public void updateImages(UploadImagesRequest request) {
        if (request.getImages().isEmpty()) {
            return;
        }
        mapper.deleteByStationCodeAndUploadTime(request.getImages().get(0).getStationCode(), request.getUploadTime());
        mapper.insertImages(request.getImages());
    }
    // 根据 station_code 和 upload_time 删除图片记录
    public int deleteImagesByStationCodeAndUploadTime(String stationCode, String uploadTime) {
        return mapper.deleteByStationCodeAndUploadTime(stationCode, uploadTime);
    }
}
