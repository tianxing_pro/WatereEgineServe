package com.gdsky.www.WaterEngineServe.service;

import com.gdsky.www.WaterEngineServe.mapper.StationRunMapper;
import com.gdsky.www.WaterEngineServe.pojo.StationRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StationRunService {

    @Autowired
    private StationRunMapper stationRunMapper;

    public List<StationRun> findByStationCode(String stationCode) {
        return stationRunMapper.findByStationCode(stationCode);
    }
}

